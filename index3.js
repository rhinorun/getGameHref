let puppeteer = require('puppeteer');
let axios = require('axios');
let url = require('url');
let fs = require('fs')

//目标: 获取https://sobooks.cc/,所有书名和电子书的链接
//进入网站,获取整个网站列表页的页数

//获取列表页的所有链接

//进入每个电子书的详情页获取下载电子书的网盘地址

//将获取的数据保存到book.txt文档里
let httpUrl = "https://down.gamersky.com/Special/bigpc/";

let nowPage = 1; //当前页数
let allCount = 0; //全部条数
let maxPage = 0; //最大页数
let nowCount =0;

let debugOptions = {
    //设置视窗宽高
    defaultViewport: {
        width: 1400,
        height: 1200
    },
    //是否有界面模式,false为有
    headless: true,
    //设置放慢每个步骤的毫秒数
    slowMo: 100,
    timeout :9999999
};

let options = {
    headless: true
};


(async function () {

    //打开列表页
    const browser = await puppeteer.launch(debugOptions);
    let page = await browser.newPage();
    await page.goto(httpUrl,{timeout: 0});

    await getAllNum();

    await getList()



    //获取
    async function getList() {
        if (!maxPage) return;
        let arr = [];
        for (let i = 1; i <= maxPage; i++) {
            arr.push(i);
        }
        for (let item of arr) {
            await pageList(item);
        }
    }

    //获取总页数
    //用了async 返回的也是一个promise对象,await的报错用try和catch接收
    async function getAllNum() {
        //倒数第二个a标签
        let allPage = await page.$eval(".contentpage span a:nth-last-child(2)", ele => ele.getAttribute("data-page"))
        // console.log(allPage)
        let pageSize = await page.$$eval(".down_con .lx0", divs => divs.length)
        // console.log(pageSize)

        allCount = pageSize * allPage;
        maxPage = allPage;

    }

    //获取当前列表
    async function pageList(num) {
        nowPage =num;
        if (num > 1) {
            await page.waitForSelector('.Mid2 .loadpic', {
                hidden: true
            });
            let allPage = await page.$$(".contentpage span a");
            for (let i of allPage) {
                const attr = await page.evaluate(el => el.getAttribute("data-page"), i);
                if (attr == num) {
                    await i.click();
                    break;
                }
            }
            await page.waitForSelector('.Mid2 .contentpaging', {
                visible: true
            });
            await page.waitFor(200)
        }

        let pageListUrl = ""
        let arrPage = await page.$$eval(".down_con .lx0 .dow a", ele => {
            let arr = []
            ele.forEach((v, i) => {
                let obj = {
                    href: v.getAttribute("href"),
                    title: v.getAttribute("title")
                }
                arr.push(obj)
            })
            console.log(arr)
            return arr
        })
        // console.log(arrPage)
        //通过获取的数组的地址和标题去获得详细地址
        for (let i of arrPage) {
            await getPageInfo(i)
        }


    }

    //跳转到详情页
    async function getPageInfo(pageObj) {
        let nPage = await browser.newPage();
        await nPage.setDefaultNavigationTimeout(0); 


        //是否使用请求拦截器
        await nPage.setRequestInterception(true);
        //监听请求事件,并对请求进行拦截
        nPage.on('request', interceptedRequest => {
            //通过url模块对请求地址进行解析
            let urlObj = url.parse(interceptedRequest.url())
            if(urlObj.hostname == "j.gamersky.com"||urlObj.hostname == "image.gamersky.com" || urlObj.hostname == "imgf.gamersky.com" || urlObj.hostname == "googleads.g.doubleclick.net" || urlObj.hostname == "hm.baidu.com"|| urlObj.hostname == "googleads.g.doubleclick.net" || urlObj.hostname == "hm.baidu.com"|| urlObj.hostname == "ja.gamersky.com"|| urlObj.hostname == "ja2.gamersky.com")
                //放弃当次请求
                interceptedRequest.abort();
            else
                interceptedRequest.continue();
        });

        await nPage.goto(pageObj.href,{timeout: 0})
        let hrefObj = await nPage.$$eval(".Download .dl_url>a", ele => {
            let obj = {
                baiduHref: '',
                btHref: ''
            }
            ele.forEach((v, i) => {
                console.log(v.getAttribute('href'))
                if (v.innerText.indexOf('百度') != -1) {
                    obj.baiduHref = v.getAttribute('href')
                } else {
                    obj.btHref = v.getAttribute('href')
                }
            })
            return obj
        })

        hrefObj.baiduHref = await getBaiduUrl(hrefObj.baiduHref);
        nowCount++;
        let content = `===第${nowPage}页-${nowCount}.游戏名:${pageObj.title}===\nbt下载:${hrefObj.btHref}\n百度下载:${hrefObj.baiduHref}\n\n`
        fs.writeFile('game.txt', content, {
            flag: "a"
        }, function () {
            console.log(pageObj.title + '已写入')
        })
        nPage.close()
    }
    //跳转到百度
    async function getBaiduUrl(href) {
        if (!href) return "无"
        let nPage = await browser.newPage();
        await nPage.setDefaultNavigationTimeout(0); 
        await nPage.goto(href,{timeout: 0});
        let url = await nPage.url();
        await nPage.close();
        return url;
    }

})()